﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Representantes.Models;
using Representantes.Interfaces;
using Dapper;
using System.Data.Common;

namespace Representantes.Repository
{
    public class RepresentanteRepository : IRepresentanteRepository
    {
        private DbConnection _connect;

        public RepresentanteRepository (DbConnection connect)
        {
            _connect = connect;
        }

        public bool Delete(int id)
        {
            try
            {
                _connect.Execute(@"DELETE FROM mega.representante
                                    WHERE id = :id"
                                 , new { id });
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteTelefone(TelefoneRepresentante telefone)
        {
            try
            {
                _connect.Execute(@"DELETE FROM MEGA.TELEFONEREPRESENTANTE
                                    WHERE id_representante = :idRepresentante
                                      AND id               = :idTelefone"
                                 , new { idRepresentante = telefone.IdRepresentante, idTelefone = telefone.Id});
                return true;
            }
            catch
            {
                return false;
            }

        }

        public Representante Get(int id)
        {
            return _connect.Query<Representante>(@"SELECT id
                                                        , rep_st_nome as nome 
                                                   FROM MEGA.REPRESENTANTE 
                                                  WHERE id = :id"
                                                 , new {id}).SingleOrDefault();

        }

        public IEnumerable<Representante> Get()
        {
            return _connect.Query<Representante>(@"SELECT id 
                                                        , rep_st_nome   as Nome 
                                                        , mun_in_codigo as IdMunicipio
                                                     FROM MEGA.REPRESENTANTE");
        }

        public IEnumerable<RepresentanteDetailDTO> GetDetailDTO()
        {
            return _connect.Query<RepresentanteDetailDTO>(@"SELECT rep.id 
                                                                 , rep.rep_st_nome       as Nome 
                                                                 , rep.rep_dt_nascimento as DataNascimento
                                                                 , mun.mun_st_nome       as NomeMunicipio
                                                              FROM MEGA.REPRESENTANTE rep
                                                        INNER JOIN mgglo.glo_municipio mun on rep.mun_in_codigo = mun.mun_in_codigo
                                                             WHERE mun.uf_st_sigla = 'PR'"); //Pequeno acoxabramento
        }

        public RepresentanteDetailDTO GetDetailDTO(int id)
        {
            //Esta query retorna erro e não consegui entender o seu funcionamento com o exemplo
            /*return _connect.Query<RepresentanteDetailDTO, IEnumerable<Telefone>, RepresentanteDetailDTO>(@"SELECT rep.id 
                                                                                                                , rep.rep_st_nome       as Nome 
                                                                                                                , rep.rep_dt_nascimento as DataNascimento
                                                                                                                , mun.mun_st_nome       as NomeMunicipio
                                                                                                                , 
                                                                                                             FROM MEGA.REPRESENTANTE rep
                                                                                                       INNER JOIN mgglo.glo_municipio mun on rep.mun_in_codigo = mun.mun_in_codigo
                                                                                                            WHERE mun.uf_st_sigla = 'PR'
                                                                                                              AND rep.id = :id" 
                                                                                                                 , (representante, Telefone) => {
                                                                                                                     representante.Telefones = GetTelefone(representante.Id);
                                                                                                                     return representante;
                                                                                                                 }
                                                                                                                 , new { id }).Single();*/
            return _connect.Query<RepresentanteDetailDTO>(@"SELECT rep.id 
                                                                 , rep.rep_st_nome       as Nome 
                                                                 , rep.rep_dt_nascimento as DataNascimento
                                                                 , mun.mun_st_nome       as NomeMunicipio
                                                             FROM MEGA.REPRESENTANTE rep
                                                       INNER JOIN mgglo.glo_municipio mun on rep.mun_in_codigo = mun.mun_in_codigo
                                                            WHERE mun.uf_st_sigla = 'PR'
                                                              AND rep.id = :id" 
                                                          , new { id }).SingleOrDefault();


        }

        public IEnumerable<TelefoneRepresentanteDTO> GetTelefone(int idRepresentante)
        {
            return _connect.Query<TelefoneRepresentanteDTO>(@"SELECT Id 
                                                                   , tel_in_numero as Numero
                                                                FROM MEGA.TELEFONEREPRESENTANTE
                                                               WHERE id_representante = :idRepresentante"
                                                            , new { idRepresentante });
        }

        public Representante Insert(Representante representante)
        {
            _connect.Execute(@"insert into mega.representante(id
                                                            , rep_st_nome
                                                            , rep_dt_nascimento
                                                            , mun_in_codigo)
                                                      values( :id
                                                            , :nome
                                                            , :nascimento
                                                            , :municipio)",
                                                    new
                                                    {
                                                        id          = representante.Id,
                                                        nome        = representante.Nome,
                                                        nascimento  = representante.DataNascimento,
                                                        municipio   = representante.IdMunicipio
                                                    });
            return representante;
        }

        public TelefoneRepresentante InsertTelefone(TelefoneRepresentante telefone)
        {
            _connect.Execute(@"INSERT INTO mega.telefonerepresentante(id_representante
                                                                    , id
                                                                    , tel_in_numero)
                                                              values( :id_representante
                                                                    , :id
                                                                    , :tel_in_numero)"
                             , new { id_representante = telefone.IdRepresentante,
                                     id = telefone.Id,
                                     tel_in_numero = telefone.Numero});
            return telefone;

        }
    }
}
