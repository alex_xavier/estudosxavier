﻿using Representantes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Representantes.Interfaces
{
    public interface IRepresentanteRepository
    {
        Representante Get(int id);
        IEnumerable<Representante> Get();
        IEnumerable<TelefoneRepresentanteDTO> GetTelefone(int idRepresentante);
        IEnumerable<RepresentanteDetailDTO> GetDetailDTO();
        RepresentanteDetailDTO GetDetailDTO(int id);
        Representante Insert(Representante representante);
        bool Delete(int id);
        TelefoneRepresentante InsertTelefone(TelefoneRepresentante telefone);
        bool DeleteTelefone(TelefoneRepresentante telefone);

    }
}
