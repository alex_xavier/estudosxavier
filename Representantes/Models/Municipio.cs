﻿namespace Representantes.Models
{
    public class Municipio
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}