﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Representantes.Models
{
    public class TelefoneRepresentanteDTO
    {
        public int Id { get; set; }
        public string Numero { get; set; }
    }
}
