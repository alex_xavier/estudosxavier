﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Representantes.Models
{
    public class Representante
    {
        public int Id { get; set; }
        public string  Nome{ get; set; }
        public DateTime DataNascimento  { get; set; }
        public int IdMunicipio { get; set; } 
    }
}
