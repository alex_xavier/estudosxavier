﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Representantes.Models
{
    public class TelefoneRepresentante
    {
        public int Id { get; set; }
        public int IdRepresentante { get; set; }
        public string Numero { get; set; }
    }
}
