﻿using Representantes.Interfaces;
using Representantes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace Representantes.Services
{
    public class RepresentanteService
    {
        private IRepresentanteRepository _repository;

        public RepresentanteService(IRepresentanteRepository repository)
        {
            _repository = repository;
        }

        public Representante Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<Representante> Get()
        {
            return _repository.Get();
        }

        public IEnumerable<RepresentanteDetailDTO> GetDetail()
        {
            IEnumerable<RepresentanteDetailDTO> representantes = _repository.GetDetailDTO();            
            for (int i = 0; i < representantes.Count(); i++)
                representantes.ElementAt(i).Telefones = _repository.GetTelefone(representantes.ElementAt(i).Id);

            return representantes;
            
        }

        public RepresentanteDetailDTO GetDetail(int Id)
        {
            RepresentanteDetailDTO representante = _repository.GetDetailDTO(Id);
            if (representante is null)
                throw new KeyNotFoundException();
            representante.Telefones = _repository.GetTelefone(representante.Id);
            return representante;
        }

        public IEnumerable<TelefoneRepresentanteDTO> GetTelefone(int idRepresentante)
        {
            return _repository.GetTelefone(idRepresentante);
        }

        public Representante Insert(Representante representante)
        {
            if (Get(representante.Id) is null)
                //Não estou validando o id no município ou se data de nascimento é nulo
                return _repository.Insert(representante);
            else
                return null;
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public bool DeleteTelefone(TelefoneRepresentante telefone)
        {
            return _repository.DeleteTelefone(telefone);
        }

        public TelefoneRepresentante InsertTelefone(TelefoneRepresentante telefone)
        {
            return _repository.InsertTelefone(telefone);
        }

        public IEnumerable<string> InsertTelefone(IEnumerable<TelefoneRepresentante> telefones)
        {
            List<String> returns = new List<String>();
            foreach (TelefoneRepresentante telefone in telefones)
            {
                try
                {
                    _repository.InsertTelefone(telefone);
                    returns.Add(String.Format("Telefone com id {0} inserido com sucesso.", telefone.Id));
                }
                catch (Exception e)
                {
                    returns.Add(String.Format("Telefone com id {0} não foi inserido, erro: {1}.", telefone.Id, e.Message));
                }
            }
            return returns;
        }
    }
}
