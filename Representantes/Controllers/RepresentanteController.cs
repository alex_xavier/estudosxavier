﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Representantes.Services;
using Representantes.Models;

namespace Representantes.Controllers
{
    [Route("api/[controller]")]
    public class RepresentanteController : Controller
    {
        private RepresentanteService _service;

        public RepresentanteController(RepresentanteService service)
        {
            _service = service;
        }
        
        // GET api/values
        [HttpGet]
        public IEnumerable<Representante> Get()
        {
            return _service.Get();
        }

        // GET representante/detail
        [HttpGet("Detail")]
        public IEnumerable<RepresentanteDetailDTO> GetDetail()
        {
            return _service.GetDetail(); ;
        }

        // GET representante/detail
        [HttpGet("telefone/{idRepresentante}")]
        public IEnumerable<TelefoneRepresentanteDTO> GetTelefone(int idRepresentante)
        {
            return _service.GetTelefone(idRepresentante); ;
        }

        // POST api/representante/insert
        [HttpPost("telefone/insert")]
        public TelefoneRepresentante PostTelefone([FromBody]TelefoneRepresentante value)
        {
            return _service.InsertTelefone(value);
        }

        [HttpPost("telefone/insertN")]
        public IEnumerable<string> PostTelefone([FromBody]IEnumerable<TelefoneRepresentante> value)
        {
            return _service.InsertTelefone(value);
        }

        // POST api/representante/insert
        [HttpPost("telefone/delete")]
        public bool DeleteTelefone([FromBody]TelefoneRepresentante value)
        {
            return _service.DeleteTelefone(value);
        }

        // GET representante/detail/6
        [HttpGet("Detail/{id}")]
        public RepresentanteDetailDTO GetDetail(int id)
        {
            return _service.GetDetail(id);
        }


        // GET api/values/5
        [HttpGet("{id}")]
        public Representante Get(int id)
        {
            return _service.Get(id);
        }

        // POST api/representante/insert
        [HttpPost("insert")]
        public Representante Post([FromBody]Representante value)
        {
            return _service.Insert(value);
        }

        // POST api/representante/insert
        [HttpPost("delete/{id}")]
        public bool PostDelete(int id)
        {
            return _service.Delete(id);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
